'use strict'

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/

const Route = use('Route')

Route.on('/').render('welcome')
Route.get('/todos', 'TodoController.index')
Route.post('/todos', 'TodoController.create')
Route.put('/todos/:id','TodoController.update')
Route.delete('/todos/:id', 'TodoController.delete')
Route.get('/todos/show', 'TodoController.show')
Route.get('/todos/view', 'TodoController.view')
Route.post('/register', 'UserController.create')
Route.post('/register/login', 'UserController.login')


