'use strict'
const Todo = use('App/Model/Todo');
const Validator = use('Validator');


class TodoController {
    * index(request, response){
        const todos = yield Todo.all();
        return response.json(todos);
    }
    
    * create(request, response){
        const validation = yield Validator.validateAll(request.all(), Todo.rules)
        if(validation.fails()){
            return response.badRequest({err:validation.messages()});
        }
        try{
            const todo = yield Todo.create({
                name:request.input('name')
            })
            response.json({msg: 'Your todo was successfully saved'})
        }
        catch(e){
            response.expectationFailed({msg:['an error occured while creating your todo', e.message]})
        }
    }
    
    * update(request, response){
        const validation = yield Validator.validateAll(request.all(), Todo.rules)
        if(validation.fails()){
            return response.badRequest({err:validation.messages()});
        }
        try{
            var id = request.params('id').id
            var todo = yield Todo.find(id)
            console.log(todo)
            todo.name = request.input('name')
            yield todo.save();
            
            //response.json({msg: 'Your todo was successfully updated'})
            response.json({todoupdate: todo})
        }
        catch(e){
            response.expectationFailed({msg:['an error occured while creating your todo', e.message]})
        }
    }
    
    * delete(request, response){
        try{
            const id = request.params('id').id
            const todo = yield Todo.find(id)
            console.log(todo)
            yield todo.delete();
            
            //response.json({msg: 'Your todo was successfully deleted'})
            response.json({tododelete: todo})
        }
        catch(e){
            response.expectationFailed({msg:['an error occured while creating your todo', e.message]})
        }
    }
    
    * show(request, response){
        yield response.sendView("welcome")
    }
    
    * view(request, response){
        const todos = yield Todo.all();
        yield response.sendView('todo', {todos: todos.toJSON()})
    }
}

module.exports = TodoController
