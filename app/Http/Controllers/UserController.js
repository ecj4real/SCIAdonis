'use strict'
const User = use('App/Model/User');
const Validator = use('Validator');

class UserController {
    * create(request, response){
//        response.json(request.all())
        const validation = yield Validator.validateAll(request.all(), User.rules)
        if(validation.fails()){
            return response.badRequest({err:validation.messages()});
        }
        try{
            const user = yield User.create({
                firstname: request.input('firstname'),
                lastname: request.input('lastname'),
                email: request.input('email'),
                password: request.input('password')
            })
            return response.json({msg: 'User successfully registered'})
        }
        catch(e){
            response.expectationFailed({msg:['an error occured while registering', e.message]})
        }
    }
    
    * login(request, response){
        const validation = yield Validator.validateAll(request.all(), User.loginRules)
        if(validation.fails()){
            return response.badRequest({err:validation.messages()});
        }
        
        try{
            const email = request.input('email')
            const password = request.input('password')
            const token = yield request.auth.attempt(email, password)
            const user = yield User.query().where('email', email).fetch()
            
            return response.json({
                user: user,
                token: token
            })
        }
        catch(e){
            response.expectationFailed({msg:['an error occured while Login', e.message]})
        }
    }
}

module.exports = UserController
