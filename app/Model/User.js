'use strict'

const Lucid = use('Lucid')

class User extends Lucid {
    static boot () {
        
        super.boot();
        this.defineHooks('beforeCreate', 'User.encryptPassword');
    }
    
    static get rules(){
        return{
            firstname: 'required',
            lastname: 'required',
            email: 'required',
            password: 'required'
        }
    }
    
    static get loginRules(){
        return{
            email: 'required|email',
            password: 'required'
        }        
    }   
}

module.exports = User
