'use strict'

const Lucid = use('Lucid')

class Todo extends Lucid {
    static get rules(){
        return{
            name: 'required'
        }
    }
}

module.exports = Todo
